import React from "react";
import PropTypes from "prop-types";

function Comment(props) {
 var theMail = `<${props.authorEmail}>`;
  return (
    <div>
      <li>
        <p>{props.content}</p>
        <p><i>{props.authorName} {theMail}</i></p>
      </li>
    </div>
  );
}

Comment.propTypes = {
  authorName: PropTypes.string.isRequired,
  authorEmail: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired
};

export default Comment;
