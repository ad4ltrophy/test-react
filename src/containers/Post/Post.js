import React from "react";
import { connect } from "react-redux";
import Comment from "../../components/Comment";
import { fetch } from "./actions";

class Post extends React.PureComponent {
  componentDidMount() {
    let postId = this.props.match.params.id;
    this.props.dispatch(fetch(postId));
  }

  renderCommentsList = () => {
      if(this.props.comments){
        return this.props.comments.map(comment => {
            return (
              <Comment
                key={comment.id}
                authorName={comment.name}
                authorEmail={comment.email}
                content={comment.body}
              />
            );
          });
      } else {
          return <div>Aucun commentaire pour le moment</div>;
      }
    
  };

  render() {
    const { post, user, comments } = this.props;
    if (post === null || user === null || comments === null) {
      return <p>Loading Post from API</p>;
    }

    return (
      <div>
        <h1>{post.title}</h1>
        <p>
          <i>
            By {user.name}, {user.company.name}, {user.address.city}
          </i>
        </p>
        <p>{post.body}</p>
        <h3>Comments</h3>
        <ul>{this.renderCommentsList()}</ul>
      </div>
    );
  }
}

export default connect(({ post }) => ({
  post: post.post,
  user: post.user,
  comments: post.comments
}))(Post);
